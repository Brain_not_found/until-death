﻿// Using Unity
using UnityEngine;

public class AudioBackgroundScript : MonoBehaviour
{
    private bool CheckedIfExist = false;

    // Ne pas supprimer le gameobject au changement de scène
    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Update()
    {
        if (!CheckedIfExist)
        {
            foreach (GameObject foundGameObject in GameObject.FindGameObjectsWithTag("AudioBackground"))
            {
                if (!foundGameObject.Equals(gameObject))
                {
                    DestroyImmediate(gameObject);
                }
            }
        }
    }
}
