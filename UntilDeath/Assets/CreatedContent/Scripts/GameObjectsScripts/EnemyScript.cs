﻿// Using System
using System;

// Using Unity
using UnityEngine;
using UnityEngine.UI;

#region Classe du GameObject
public class EnemyScript : MonoBehaviour
{
    #region Properties
    public string Name { get; set; }
    public int Health { get; set; }
    public int MaxHealth { get; set; }
    public int Damages { get; set; }

    public HealthBarScript healthBar;
    public Text nameText;
    public Sprite[] images;
    #endregion

    #region Constructor
    public void Initialize(string name, int health, int damages)
    {
        Name = name;
        Health = health;
        MaxHealth = health;
        Damages = damages;

        healthBar.SetMaxHealth(Health);
        nameText.text = Name;

        switch(name)
        {
            case "Zombie":
                gameObject.GetComponent<SpriteRenderer>().sprite = images[0];
                break;
            case "Chien zombie":
                gameObject.GetComponent<SpriteRenderer>().sprite = images[1];
                break;
            case "Zombie tronçonneuse":
                gameObject.GetComponent<SpriteRenderer>().sprite = images[3];
                break;
            case "Hunter":
                gameObject.GetComponent<SpriteRenderer>().sprite = images[2];
                break;
            case "Tyran":
                gameObject.GetComponent<SpriteRenderer>().sprite = images[4];
                break;
        }
    }
    #endregion

    #region Functions
    /// <summary>
    /// Modifie la propriété Health par rapport à la valeur damages
    /// </summary>
    /// <param name="damages"></param>
    public void TakeDamages(int damages)
    {
        if(Health > damages)
        {
            Health -= damages;
            healthBar.SetHealth(Health);
        }
        else
        {
            MainCharacterScript.IsFighting = false;
            MainCharacterScript.CurrentGameProgressScript.CharacterScore += MaxHealth;

            Destroy(gameObject);
        }
    }
    /// <summary>
    /// Appel fonction TakeDamages de MainCharacterScript + envoie dégats de l'objet
    /// </summary>
    public void Attack()
    {
        MainCharacterScript character = GameObject.FindGameObjectWithTag("Character").GetComponent<MainCharacterScript>();
        character.TakeDamages(Damages);
    }
    #endregion
}
#endregion

#region Classes modèles
[Serializable]
public class EnemyModel
{
    #region Properties
    public string Name { get; set; }
    public int Health { get; set; }
    public int Damages { get; set; }
    #endregion

    #region Constructors
    public EnemyModel() { }
    public EnemyModel(string name, int health, int damages)
    {
        this.Name = name;
        this.Health = health;
        this.Damages = damages;
    }
    #endregion
}

[Serializable]
public class EnemyPosition
{
    #region Properties
    public string Name { get; set; }
    public float X { get; set; }
    public float Y { get; set; }
    #endregion

    #region Constructor
    public EnemyPosition() { }
    public EnemyPosition(string name, float x, float y)
    {
        this.Name = name;
        this.X = x;
        this.Y = y;
    }
    #endregion
}
#endregion