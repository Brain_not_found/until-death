﻿// Using System
using System;
using System.Collections.Generic;
using System.Linq;

// Using Unity
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Using Project
using Assets.CreatedContent.Scripts.Utilities;

#region Classe du gameObject
public class MainCharacterScript : MonoBehaviour
{
    #region Properties
    public static readonly float CharacterSpeed = 5f;

    public static string Name { get; set; }
    public static int MaxHealth { get; set; }
    public static int Health { get; set; }
    public static int EnergyAmount { get; set; }
    public static int BuildingFloorPosition { get; set; }
    public static double DamageReductionPercentage { get; set; }
    public static bool IsFighting { get; set; }
    public static bool GoToUpperFloor { get; set; }
    public static bool HasEnteredBuilding { get; set; }
    public static bool ExitBuilding { get; set; }
    public static GameObject EquippedWeapon { get; set; }
    public static List<EquipmentObjectClass> EquipmentObjects { get; set; }
    public static List<StateClass> CharacterStates { get; set; }
    public static List<GameObject> WeaponList { get; set; }
    public static GameProgressScript CurrentGameProgressScript { get; set; }
    private static float XPositionTarget { get; set; }
    public static bool CharacterHasToMove { get; set; }
    public static int IndexEquippedWeapon { get; set; }

    public Text nameText;
    public HealthBarScript healthBar;
    public Sprite[] weaponPictures;
    #endregion

    #region Constructor
    public void Initialize(string name, int health, int energyAmount)
    {
        Name = name;
        Health = health;
        MaxHealth = health;
        EnergyAmount = energyAmount;
        DamageReductionPercentage = 0;
        BuildingFloorPosition = 0;
        IndexEquippedWeapon = 0;
        CharacterHasToMove = false;

        healthBar.SetMaxHealth(MaxHealth);
        nameText.text = Name;

        EquipmentObjects = new List<EquipmentObjectClass>();
        CharacterStates = new List<StateClass>();
        WeaponList = new List<GameObject>();
    }
    #endregion

    #region Unity Functions
    // Permet au gameObject de ne pas être détruit lors du changement de scènes
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        Debug.Log(IsFighting);
        if (CharacterHasToMove)
        {
            // Déplacement du personnage vers sa cible
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(XPositionTarget, transform.position.y, transform.position.z), CharacterSpeed * Time.deltaTime);
        }
    }
    #endregion

    #region Functions
    /// <summary>
    /// Change l'arme actuellement équipée
    /// </summary>
    public void SwapWeapon()
    {
        Debug.Log("Arme : " + EquippedWeapon);
        if (EquippedWeapon != null)
        {
            if (WeaponList.IndexOf(EquippedWeapon) + 1 < WeaponList.Count)
                EquippedWeapon = WeaponList[WeaponList.IndexOf(EquippedWeapon) + 1];
            else
                EquippedWeapon = WeaponList[0];
        }
        else
        {
            if (WeaponList.Count > 0)
                EquippedWeapon = WeaponList[0];
        }

        IndexEquippedWeapon = WeaponList.IndexOf(EquippedWeapon);

        DisplayWeaponPicture();
    }

    /// <summary>
    /// Cherche et détruit une clé dans l'inventaire du personnage
    /// </summary>
    /// <returns>boolean si le personnage a une clé ou non</returns>
    public bool TryUseKey()
    {
        // Tentative de récupération d'une clé dans la liste d'objets du personnage
        EquipmentObjectClass potentialKey = EquipmentObjects.Any(obj => obj.Name == "Key")
            ? EquipmentObjects.Where(obj => obj.Name == "Key").First()
            : null;

        if (potentialKey != null)
        {
            // Si le personnage possède une clé
            EquipmentObjects.Remove(potentialKey);
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Consommation d'un objet et regain de PV / Énergie
    /// </summary>
    /// <param name="objectToSearch"></param>
    public void ConsumeObject(EquipmentObjectModel objectToSearch)
    {
        // Si l'objet est bien un consommable
        if (objectToSearch.Type == "Consommable")
        {
            // Recherche dans la liste d'objets pour un qui a le même nom
            if (EquipmentObjects.Any(obj => obj.Name == objectToSearch.Name))
            {
                // Récupération de l'objet
                EquipmentObjectClass equipmentObject = EquipmentObjects.Where(obj => obj.Name == objectToSearch.Name).FirstOrDefault();

                // Si elle a été faite avec succés
                if (equipmentObject != null)
                {
                    // Différenciation de la boisson énergisante
                    if (equipmentObject.Id != "EnergyDrink")
                    {
                        // Gain de vie de la quantité rendue par l'objet
                        GainLife(equipmentObject.Gain);
                    }
                    else
                    {
                        // Perte de 1 ou 2 points de vie (aléatoire)
                        TakeDamages(new System.Random().Next(1, 3));

                        // Gain d'énergie rendue par l'objet
                        EnergyAmount += equipmentObject.Gain;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Modifie la propriété Health par rapport à la valeur damages
    /// </summary>
    /// <param name="damages"></param>
    public void TakeDamages(int damages)
    {
        if (Health > damages)
        {
            Health -= damages;
            healthBar.SetHealth(Health);
        }
        else
        {
            CurrentGameProgressScript.PlayerDeath(gameObject);
        }
    }

    /// <summary>
    /// Modifie la propriété Health par rapport à la valeur gainValue
    /// </summary>
    /// <param name="gainValue"></param>
    public void GainLife(int gainValue)
    {
        Health = (Health + gainValue) > MaxHealth ? MaxHealth : Health + gainValue; 
    }

    /// <summary>
    /// Définit une position vers laquelle le personnage doit se déplacer
    /// </summary>
    /// <param name="xPosition"></param>
    public void MoveToPosition(float xPosition)
    {
        XPositionTarget = xPosition;
        CharacterHasToMove = true;
    }

    /// <summary>
    /// Prépare les variables du script pour l'entrée dans un nouveau bâtiment + tp le personnage
    /// </summary>
    public void EnterBuilding()
    {
        CharacterHasToMove = false;

        HasEnteredBuilding = true;
        IsFighting = true;
        EquippedWeapon = WeaponList[IndexEquippedWeapon];

        // Téléportation et redimension du personnage
        gameObject.transform.localScale = new Vector3(2, 2, gameObject.transform.localScale.z);
        gameObject.transform.position = new Vector3(-6.75f, 8.945f, -1);
        Transform canvasTransform = gameObject.transform.Find("Canvas");
        canvasTransform.localScale = new Vector3(Math.Abs(canvasTransform.localScale.x), canvasTransform.localScale.y, canvasTransform.localScale.z);

        // Déplacement de la caméra
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScript>().ChangeCameraPosition(0, 10f);

        CurrentGameProgressScript.ActiveInsideActions();
    }

    public void EscapeFromFight()
    {
        Debug.Log("oui");
        Debug.Log(BuildingFloorPosition);
        IsFighting = false;

        if (BuildingFloorPosition == 0)
        {
            Debug.Log("Se barre");
            ExitBuilding = true;
            MoveToPosition(-10.2f);
        } 
        else
        {
            Debug.Log("Descend");
            CurrentGameProgressScript.GoToDoor(false);
        }
    }

    public void ExitingBuilding()
    {
        CharacterHasToMove = false;
        ExitBuilding = false;
        CurrentGameProgressScript.CreateBuildingScene(false);
    }

    public void SetProgressScript(GameProgressScript gameProgressScript)
    {
        CurrentGameProgressScript = gameProgressScript;
    }

    public void DisplayWeaponPicture()
    {
        // Récupération de la zone de display de l'arme
        GameObject weaponDisplay = GameObject.FindGameObjectsWithTag("EquippedWeaponDisplay")[0].gameObject;

        Text weaponNameDisplay = weaponDisplay.GetComponentsInChildren<Transform>().Single(tran => tran.name == "WeaponNameDisplay").gameObject.GetComponent<Text>();
        Image weaponSpriteDisplay = weaponDisplay.GetComponentsInChildren<Transform>().Single(tran => tran.name == "WeaponDisplay").gameObject.GetComponent<Image>();

        Sprite spriteToUse = null;
        float zoom = 1;

        switch (EquippedWeapon.GetComponent<WeaponScript>().Id)
        {
            case "Knife":
                spriteToUse = weaponPictures[0];
                zoom = 3;
                break;
            case "Pistol":
                spriteToUse = weaponPictures[1];
                zoom = 3.5f;
                break;
            case "Shotgun":
                spriteToUse = weaponPictures[2];
                zoom = 2f;
                break;
            case "AssultRifle":
                spriteToUse = weaponPictures[3];
                zoom = 1.5f;
                break;
            case "Sniper":
                spriteToUse = weaponPictures[4];
                zoom = 1.2f;
                break;
            case "FlameThrower":
                spriteToUse = weaponPictures[5];
                zoom = 1.5f;
                break;
            default:
                spriteToUse = weaponPictures[0];
                zoom = 3;
                break;
        }

        weaponNameDisplay.text = "Arme équipée : " + EquippedWeapon.GetComponent<WeaponScript>().Name;
        weaponSpriteDisplay.rectTransform.sizeDelta = new Vector2(spriteToUse.bounds.size.x * 100, spriteToUse.bounds.size.y * 100);
        weaponSpriteDisplay.rectTransform.localScale = new Vector3(zoom, zoom, 1);
        weaponSpriteDisplay.sprite = spriteToUse;
    }

    /*public void PickupWeaponFromContainer()
    {
        ContainerScript floorContainer = GameObject.FindGameObjectWithTag("Building").GetComponent<BuildingScript>().FloorList[BuildingFloorPosition].Container;

        if (floorContainer != null)
        {
            if (floorContainer.Weapon != null)
            {
                string weaponName = floorContainer.Weapon.GetComponent<WeaponScript>().Name;
                WeaponList.Add(floorContainer.ExtractWeaponFromContainer());
                CurrentGameProgressScript.DisplayAlertMessage("Vous avez récupéré : " + weaponName);
            }
        }
    }*/
    #endregion

    #region Events
    public void OnCollisionEnter2D(Collision2D collision)
    {
        // Arrêt du déplacement
        /*CharacterHasToMove = false;
        XPositionTarget = gameObject.transform.position.x;*/

        if (!HasEnteredBuilding)
        {
            EnterBuilding();
        }
        else if (ExitBuilding)
        {
            ExitingBuilding();
        }
        else
        {
            // Changement d'étage
            if (GoToUpperFloor)
                CurrentGameProgressScript.ChangeFloor(1);
            else
                CurrentGameProgressScript.ChangeFloor(-1);
        }
    }
    #endregion
}
#endregion

#region Classe modèle
[Serializable]
public class MainCharacterModel
{
    #region Properties
    public string Name { get; set; }
    public int Health { get; set; }
    public int EnergyAmount { get; set; }
    #endregion

    #region Constructors
    public MainCharacterModel() { }
    public MainCharacterModel(string name, int health, int energyAmount)
    {
        this.Name = name;
        this.Health = health;
        this.EnergyAmount = energyAmount;
    }
    #endregion
}
#endregion