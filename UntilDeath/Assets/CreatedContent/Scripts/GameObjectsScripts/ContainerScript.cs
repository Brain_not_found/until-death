﻿// Using System
using System;
using System.Collections.Generic;

// Using Unity
using UnityEngine;

#region Classe du GameObject
public class ContainerScript : MonoBehaviour
{
    #region Properties
    public string Name { get; set; }
    public int StorageCapacity { get; set; }
    public bool HasLockOnIt { get; set; }
    public List<EquipmentObjectClass> ListEquipmentObjects { get; set; }
    public GameObject Weapon { get; set; }
    #endregion

    #region Constructor
    public void Initialize(string name, int storageCapacity, List<EquipmentObjectClass> listEquipmentObjects, GameObject weapon)
    {
        this.Name = name;
        this.StorageCapacity = storageCapacity;
        this.ListEquipmentObjects = listEquipmentObjects;
        this.Weapon = weapon;

        switch (this.Name)
        {
            case "Sac":
                this.HasLockOnIt = false;
                break;
            case "Armoire":
                this.HasLockOnIt = new System.Random().Next(1) == 0 ? true : false;
                break;
            case "Coffre":
                this.HasLockOnIt = true;
                break;
        }
    }
    #endregion

    #region Function
    /// <summary>
    /// Extraction de l'arme du conteneur
    /// </summary>
    /// <returns></returns>
    public GameObject ExtractWeaponFromContainer()
    {
        GameObject tmpWeapon = Weapon;
        Weapon = null;
        return tmpWeapon;
    }

    /// <summary>
    /// Extraction d'item du conteneur
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public EquipmentObjectClass ExtractItemFromContainer(string name)
    {
        int index = ListEquipmentObjects.FindIndex(equipmentObject => equipmentObject.Name == name);

        EquipmentObjectClass tmpEquipmentObject = ListEquipmentObjects[index];

        ListEquipmentObjects.RemoveAt(index);

        return tmpEquipmentObject;
    }
    #endregion
}
#endregion

#region Classes modèles
[Serializable]
public class ContainerModel
{
    #region Properties
    public string Name { get; set; }
    public int StorageCapacity { get; set; }
    #endregion

    #region Constructors
    public ContainerModel() { }
    public ContainerModel(string name, int storageCapactity)
    {
        this.Name = name;
        this.StorageCapacity = storageCapactity;
    }
    #endregion
}

[Serializable]
public class ContainerPosition
{
    #region Properties
    public string Name { get; set; }
    public float X { get; set; }
    public float Y { get; set; }
    #endregion

    #region Constructor
    public ContainerPosition() { }
    public ContainerPosition(string name, float x, float y)
    {
        this.Name = name;
        this.X = x;
        this.Y = y;
    }
    #endregion
}
#endregion