﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public void ChangeCameraPosition(float x, float y)
    {
        gameObject.transform.position = new Vector3(x, y, -10f);
    }
}
