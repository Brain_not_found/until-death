﻿// Using System
using System;
using System.Linq;

// Using Unity
using UnityEngine;

#region Classe du GameObject
[Serializable]
public class FloorScript : MonoBehaviour
{
    #region Properties

    // Attribution des valeurs via Unity Editor
    public Sprite[] FloorImages;

    public int FloorNumber { get; set; }
    public EnemyScript EnemyScript { get; set; }
    public ContainerScript Container { get; set; }

    private Vector2 ContainerGenerationPosition { get; set; }
    private Vector2 EnemyGenerationPosition { get; set; }
    #endregion

    #region Unity Functions
    public void Start()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = FloorImages[FloorNumber];
    }
    #endregion

    #region Constructor
    public void Initialize(int floorNumber, int totalFloorsNumber)
    {
        this.FloorNumber = floorNumber;

        // Récupération des models adaptés à l'étage
        ContainerModel containerModel = GetAdaptedContainer(totalFloorsNumber);
        EnemyModel enemyModel = GetAdaptedEnemy(totalFloorsNumber);

        WeaponModel randomWeaponModel;
        do
        {
            // Récupération d'une arme random
            randomWeaponModel = GetDataFromJson.weaponModelsList[new System.Random().Next(0, GetDataFromJson.weaponModelsList.Count)];
        } while (randomWeaponModel.Id != "Knife");

        // Création de l'arme
        GameObject randomWeapon = ObjectFactory.CreateWeapon(randomWeaponModel.Name, randomWeaponModel.Damages, randomWeaponModel.MunitionAmount, randomWeaponModel.ChargerLength, randomWeaponModel.ChargerMunitionAmout, randomWeaponModel.Id);

        // Création des objets depuis les données des models
        this.EnemyScript = ObjectFactory.CreateEnemy(enemyModel.Name, enemyModel.Health, enemyModel.Damages, EnemyGenerationPosition);
        this.Container = ObjectFactory.CreateContainer(containerModel.Name, containerModel.StorageCapacity, null, randomWeapon, ContainerGenerationPosition);

        // Retourner l'ennemi si c'est le rez-de-chaussée
        if (floorNumber == 1)
        {
            this.EnemyScript.transform.localScale = new Vector3(-EnemyScript.transform.localScale.x, EnemyScript.transform.localScale.y, EnemyScript.transform.localScale.z);

            Transform canvasTransform = this.EnemyScript.transform.Find("Canvas");
            canvasTransform.localScale = new Vector3(-canvasTransform.localScale.x, canvasTransform.localScale.y, canvasTransform.localScale.z);
        }
    }

    /// <summary>
    /// Selon le numéro de l'étage renvoie un model de sac, coffre ou armoire
    /// </summary>
    /// <returns></returns>
    private ContainerModel GetAdaptedContainer(int totalFloorsNumber)
    {
        if (FloorNumber < 1)
        {
            return null;
        }
        else if (FloorNumber == 1)
        {
            // Attribution de la position de création du container
            ContainerPosition containerPosition = GetDataFromJson.containerPositionsList.Single(contPos => contPos.Name == "Sac");
            ContainerGenerationPosition = new Vector2(containerPosition.X, containerPosition.Y);

            return GetDataFromJson.containerModelsList.Single(cont => cont.Name == "Sac");
        }
        else if (FloorNumber < totalFloorsNumber)
        {
            // 80% de chances de renvoyer une armoire
            return new System.Random().Next(100) >= 80
                ? GetDataFromJson.containerModelsList.Single(cont => cont.Name == "Sac")
                : GetDataFromJson.containerModelsList.Single(cont => cont.Name == "Armoire");
        }
        else
        {
            // Attribution de la position de création du container
            ContainerPosition containerPosition = GetDataFromJson.containerPositionsList.Single(contPos => contPos.Name == "Coffre");
            ContainerGenerationPosition = new Vector2(containerPosition.X, containerPosition.Y);

            return GetDataFromJson.containerModelsList.Single(cont => cont.Name == "Coffre");
        }
    }

    /// <summary>
    /// Selon le numéro de l'étage renvoie un model d'ennemi correspondant à un ennemi standard ou un boss
    /// </summary>
    /// <returns></returns>
    private EnemyModel GetAdaptedEnemy(int totalFloorsNumber)
    {
        // Si le numéro d'étage ne correspond pas au dernier du bâtiment
        if (FloorNumber != totalFloorsNumber)
        {
            // Attribution de la position de création de l'ennemi
            EnemyPosition enemyPosition = GetDataFromJson.enemyPositionsList.Single(contPos => contPos.Name == "Floor1Enemy");
            EnemyGenerationPosition = new Vector2(enemyPosition.X, enemyPosition.Y);

            return GetDataFromJson.enemyModelsList[new System.Random().Next(GetDataFromJson.enemyModelsList.Count)];
        }
        else
        {
            // Attribution de la position de création de l'ennemi
            EnemyPosition enemyPosition = GetDataFromJson.enemyPositionsList.Single(contPos => contPos.Name == "Floor2Enemy");
            EnemyGenerationPosition = new Vector2(enemyPosition.X, enemyPosition.Y);

            return GetDataFromJson.bossModelsList[new System.Random().Next(GetDataFromJson.bossModelsList.Count)];
        }
    }
    #endregion
}
#endregion

#region Classe modèle
[Serializable]
public class FloorModel
{
    #region Properties
    public int FloorNumber { get; set; }
    #endregion

    #region Constructors
    public FloorModel() { }
    public FloorModel(int floorNumber)
    {
        this.FloorNumber = floorNumber;
    }
    #endregion
}
#endregion