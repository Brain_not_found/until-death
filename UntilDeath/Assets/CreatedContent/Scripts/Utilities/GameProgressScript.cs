﻿// Using System
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

// using Unity
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.CreatedContent.Scripts.Utilities
{
    public class GameProgressScript : MonoBehaviour
    {

        #region Properties
        private static MainCharacterScript MainCharacter { get; set; }
        private static BuildingScript BuildingScript { get; set; }
        private GameObject InsideActions { get; set; }
        private GameObject MortMenu { get; set; }
        private GameObject Alertmessage { get; set; }

        public int CharacterScore { get; set; }
        public static string ContentFromDB { get; set; }
        public static List<ScoreFromDB> ScoreFromDBs { get; set; }

        private bool CheckedIfExist = false;
        private static bool IsFirstTimeSceneCreated { get; set; }
        #endregion

        #region Unity Functions
        public void Start()
        {
            SceneManager.activeSceneChanged += ChangedActiveScene;
        }

        public void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        public void Update()
        {
            if (!CheckedIfExist)
            {
                foreach (GameObject foundGameObject in GameObject.FindGameObjectsWithTag("GameProgress"))
                {
                    if (!foundGameObject.Equals(gameObject))
                    {
                        DestroyImmediate(gameObject);
                    }
                }
            }
        }
        #endregion

        #region Functions
        public void CreateBuildingScene(bool isFirstTimeBuildingScene)
        {
            // Création du personnage si c'est la première scène créée
            if (isFirstTimeBuildingScene)
            {
                IsFirstTimeSceneCreated = true;
            }

            SceneManager.LoadScene(1);
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void GoToDoor(bool goToUpperFloor)
        {
            // Redimension et orientation du personnage
            if (MainCharacterScript.HasEnteredBuilding)
            {
                GameObject.FindGameObjectWithTag("Character").transform.localScale = new Vector3(2, 2, GameObject.FindGameObjectWithTag("Character").transform.localScale.z);
                Transform canvasTransform = GameObject.FindGameObjectWithTag("Character").transform.Find("Canvas");
                canvasTransform.localScale = new Vector3(Math.Abs(canvasTransform.localScale.x), canvasTransform.localScale.y, canvasTransform.localScale.z);
            }

            // Définition de l'étage où envoyer le personnage
            MainCharacterScript.GoToUpperFloor = goToUpperFloor;

            // Mise en mouvement du personnage
            GameObject.FindGameObjectWithTag("Character").GetComponent<MainCharacterScript>().MoveToPosition(8.8f);
        }

        public void ChangeFloor(int numberToChange)
        {
            MainCharacterScript.CharacterHasToMove = false;

            Debug.Log("buildingPos : " + MainCharacterScript.BuildingFloorPosition);
            Debug.Log("nbToChange : " + numberToChange);
            // Changement de la référence de position dans les étages
            MainCharacterScript.BuildingFloorPosition += numberToChange;

            ActivateButtons();

            // Déplacement du personnage
            GameObject.FindGameObjectWithTag("Character").transform.position = new Vector3(GameObject.FindGameObjectWithTag("Character").transform.position.x - 1.5f, GameObject.FindGameObjectWithTag("Character").transform.position.y + (numberToChange * 10f), -1);
            GameObject.FindGameObjectWithTag("Character").transform.localScale = new Vector3(-2, 2, GameObject.FindGameObjectWithTag("Character").transform.localScale.z);
            Transform canvasTransform = GameObject.FindGameObjectWithTag("Character").transform.Find("Canvas");
            canvasTransform.localScale = new Vector3(-Math.Abs(canvasTransform.localScale.x), canvasTransform.localScale.y, canvasTransform.localScale.z);

            // Déplacement de la caméra
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScript>().ChangeCameraPosition(0, 10 + (MainCharacterScript.BuildingFloorPosition * 10f));
        }

        public void SpawnCharacterOutside()
        {
            // Déplacement du personnage pour simuler l'apparition devant le bâtiment
            GameObject.FindGameObjectWithTag("Character").transform.localScale = new Vector3(1, 1, GameObject.FindGameObjectWithTag("Character").transform.localScale.z);
            GameObject.FindGameObjectWithTag("Character").transform.position = new Vector3(-7.5f, -2.3f, -1f);
            Transform canvasTransform = GameObject.FindGameObjectWithTag("Character").transform.Find("Canvas");
            canvasTransform.localScale = new Vector3(Math.Abs(canvasTransform.localScale.x), canvasTransform.localScale.y, canvasTransform.localScale.z);
        }

        public void ActiveInsideActions()
        {
            // Activer visuellement les boutons des actions intérieures
            InsideActions.SetActive(true);
            GameObject.FindGameObjectWithTag("Character").GetComponent<MainCharacterScript>().DisplayWeaponPicture();
            ActivateButtons();
        }

        public void AttackEnemy()
        {
            // Attribution de l'ennemi
            EnemyScript enemy = BuildingScript.FloorList[MainCharacterScript.BuildingFloorPosition].EnemyScript;

            // Inflige les dégats à l'ennemi
            enemy.TakeDamages(MainCharacterScript.EquippedWeapon.GetComponent<WeaponScript>().Damages);

            // Si l'ennemi ne s'est pas fait tuer
            if (MainCharacterScript.IsFighting)
            {
                // Attaque de l'ennemi
                enemy.Attack();
            }
            else
            {
                // Désactivation des actions du personnage si l'ennemi est mort
                GameObject.Find("AttackButton").GetComponent<Button>().interactable = false;
                GameObject.Find("EscapeFromFight").GetComponent<Button>().interactable = false;
                GameObject.Find("ChangeBuilding").GetComponent<Button>().interactable = true;


                // Activation des boutons pour changer d'étage selon s'il est en bas, en haut ou autre d'un bâtiment
                if (MainCharacterScript.BuildingFloorPosition == 0)
                {
                    GameObject.Find("GoUpperFloor").GetComponent<Button>().interactable = true;
                    GameObject.Find("GoDownFloor").GetComponent<Button>().interactable = false;
                }
                else if (MainCharacterScript.BuildingFloorPosition < BuildingScript.FloorsNumber - 1)
                {
                    GameObject.Find("GoUpperFloor").GetComponent<Button>().interactable = true;
                    GameObject.Find("GoDownFloor").GetComponent<Button>().interactable = true;
                }
                else
                {
                    GameObject.Find("GoUpperFloor").GetComponent<Button>().interactable = false;
                    GameObject.Find("GoDownFloor").GetComponent<Button>().interactable = true;
                }
            }
        }

        public void ActivateButtons()
        {
            // Définition de si le personnage affronte un ennemi à l'étage où il est
            if (BuildingScript.FloorList[MainCharacterScript.BuildingFloorPosition].EnemyScript != null)
            {
                MainCharacterScript.IsFighting = true;
                GameObject.Find("AttackButton").GetComponent<Button>().interactable = true;
                GameObject.Find("EscapeFromFight").GetComponent<Button>().interactable = true;
                GameObject.Find("ChangeBuilding").GetComponent<Button>().interactable = false;
                GameObject.Find("GoUpperFloor").GetComponent<Button>().interactable = false;
                GameObject.Find("GoDownFloor").GetComponent<Button>().interactable = false;
            }
            else
            {
                MainCharacterScript.IsFighting = false;
                GameObject.Find("AttackButton").GetComponent<Button>().interactable = false;
                GameObject.Find("EscapeFromFight").GetComponent<Button>().interactable = false;
                GameObject.Find("ChangeBuilding").GetComponent<Button>().interactable = true;

                // Activation des boutons pour changer d'étage selon s'il est en bas, en haut ou autre d'un bâtiment
                if (MainCharacterScript.BuildingFloorPosition == 0)
                {
                    GameObject.Find("GoUpperFloor").GetComponent<Button>().interactable = true;
                    GameObject.Find("GoDownFloor").GetComponent<Button>().interactable = false;
                }
                else if (MainCharacterScript.BuildingFloorPosition < BuildingScript.FloorsNumber - 1)
                {
                    GameObject.Find("GoUpperFloor").GetComponent<Button>().interactable = true;
                    GameObject.Find("GoDownFloor").GetComponent<Button>().interactable = true;
                }
                else
                {
                    GameObject.Find("GoUpperFloor").GetComponent<Button>().interactable = false;
                    GameObject.Find("GoDownFloor").GetComponent<Button>().interactable = true;
                }
            }
        }

        public void PlayerDeath(GameObject player)
        {
            foreach(GameObject foundGameObject in FindObjectsOfType<GameObject>())
            {
                // Destruction des gameObjects clonés (bâtiment, ennemis, personnage, coffres)
                if (foundGameObject.name.Contains("(Clone)"))
                {
                    Destroy(foundGameObject);
                }
                else if (foundGameObject.name == "OutsideBuildingActions" || foundGameObject.name == "InsideBuildingActions")
                {
                    foundGameObject.SetActive(false);
                }
            }

            MortMenu.SetActive(true);

            GameObject.FindGameObjectWithTag("PlayerDataField").transform.Find("ScoreField").GetComponent<Text>().text = CharacterScore.ToString();

            FillBestScores();
        }

        public void FillBestScores()
        {
            GameObject tableHighScores = GameObject.FindGameObjectWithTag("TableHighScores");
            List<GameObject> highScoresDisplay = GameObject.FindGameObjectsWithTag("ScoreDisplay").ToList();

            RequestDBHighScores();

            // Tri des scores pour les faire apparaître de manière croissante
            ScoreFromDBs = ScoreFromDBs.OrderByDescending(score => score.Score).ToList();

            ScoreFromDBs.ForEach(score => Debug.Log(score.Name + " : " + score.Score + ", le " + score.DateAndTime));

            for (int i = 0; i < ScoreFromDBs.Count; i++)
            {
                highScoresDisplay.Single(hs => hs.name == (i + 1).ToString()).GetComponent<Text>().text = ScoreFromDBs[i].Name + " : " + ScoreFromDBs[i].Score;
            }
        }

        public static void RequestDBHighScores()
        {
            ScoreFromDBs = TransformDataFromDB<List<ScoreFromDB>>("https://portfoliokarl.000webhostapp.com/Pages/GetDBHighScores.php");
        }

        private static T TransformDataFromDB<T>(string requestUrl)
        {
            // Traitement de la requête
            IEnumerator req = GetDataFromDB(requestUrl);

            // Blocage tant que la requête n'est pas terminée
            while (req.MoveNext()) ;

            try
            {
                return JsonConvert.DeserializeObject<T>(ContentFromDB);
            }
            catch (Exception exc)
            {
                // Log
                Debug.Log("Erreur lors de la conversion du JSON de la requête web : " + exc.Message);

                // Retourne une valeur vide
                return default(T);
            }
        }

        private static IEnumerator GetDataFromDB(string requestUrl)
        {
            // Log
            Debug.Log("Début de la requête GetDataFromDB() à l'adresse : " + requestUrl);

            // Création de la requête
            UnityWebRequest webRequest = UnityWebRequest.Get(requestUrl);

            // Envoi de la requête
            yield return webRequest.SendWebRequest();

            // Log
            Debug.Log("Progression de la requête : ");

            // Renvoie true tant que la requête n'est pas terminée
            while (!webRequest.isDone)
            {
                Debug.Log(webRequest.downloadProgress * 100 + "%");

                yield return null;
            }

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                // Log
                Debug.Log("Requête effectuée avec succés");

                // Attribution de la valeur de retour de requête
                ContentFromDB = webRequest.downloadHandler.text;
            }
        }

        /*public void GoToMainMenu()
        {
            SceneManager.LoadScene(0);
        }*/

        /*public void DisplayAlertMessage(string text)
        {
            Alertmessage.transform.Find("MessageDisplay").GetComponent<Text>().text = text;

            Alertmessage.SetActive(true);
        }*/
        #endregion

        #region Events
        private void ChangedActiveScene(Scene current, Scene next)
        {
            Debug.Log("IsFirstTimeScene : " + IsFirstTimeSceneCreated);
            if (next.buildIndex == 1)
            {
                if (IsFirstTimeSceneCreated)
                {
                    Debug.Log("Création perso");
                    // Récupération du personnage
                    MainCharacterModel characterModel = GetDataFromJson.mainCharacterModelsList.Single(mainCharac => mainCharac.Name == "John");

                    // Création du personnage
                    MainCharacter = ObjectFactory.CreateCharacter(characterModel.Name, characterModel.Health, characterModel.EnergyAmount);
                    GameObject.FindGameObjectWithTag("Character").GetComponent<MainCharacterScript>().SetProgressScript(gameObject.GetComponent<GameProgressScript>());

                    // Création du couteau du personnage
                    /*WeaponModel knifeModel = GetDataFromJson.weaponModelsList.Single(weap => weap.Id == "Knife");
                    GameObject knife = ObjectFactory.CreateWeapon(knifeModel.Name, knifeModel.Damages, knifeModel.MunitionAmount, knifeModel.ChargerLength, knifeModel.ChargerMunitionAmout, knifeModel.Id);

                    // Attribution du couteau au personnage
                    MainCharacterScript.WeaponList.Add(knife);
                    MainCharacterScript.EquippedWeapon = knife;*/

                    GetDataFromJson.weaponModelsList.ForEach(weapModel =>
                    {
                        GameObject weap = ObjectFactory.CreateWeapon(weapModel.Name, weapModel.Damages, weapModel.MunitionAmount, weapModel.ChargerLength, weapModel.ChargerMunitionAmout, weapModel.Id);
                        MainCharacterScript.WeaponList.Add(weap);
                    });

                    IsFirstTimeSceneCreated = false;
                }

                // Récupération du bouton d'attaque puis désactivation
                InsideActions = GameObject.Find("InsideBuildingActions").gameObject;
                MortMenu = GameObject.Find("MortMenu").gameObject;
                Alertmessage = GameObject.FindGameObjectWithTag("AlertMessage");

                InsideActions.SetActive(false);
                MortMenu.SetActive(false);
                Alertmessage.SetActive(false);

                // Récupération du bâtiment
                BuildingModel buildingModel = GetDataFromJson.buildingModelsList.Where(build => build.Name == "Commissariat").First();

                // Création du bâtiment
                BuildingScript = ObjectFactory.CreateBuilding(buildingModel.Name, buildingModel.FloorsNumber);
                MainCharacterScript.HasEnteredBuilding = false;

                // Attribution de la caméra
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScript>();

                MainCharacterScript.WeaponList.Clear();

                GetDataFromJson.weaponModelsList.ForEach(weapModel =>
                {
                    GameObject weap = ObjectFactory.CreateWeapon(weapModel.Name, weapModel.Damages, weapModel.MunitionAmount, weapModel.ChargerLength, weapModel.ChargerMunitionAmout, weapModel.Id);
                    MainCharacterScript.WeaponList.Add(weap);
                });

                SpawnCharacterOutside();
            }
        }
        #endregion
    }
}
