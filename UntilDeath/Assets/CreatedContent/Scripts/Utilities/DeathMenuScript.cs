﻿// Using system
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;

// Using unity
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using Assets.CreatedContent.Scripts.Utilities;

public class DeathMenuScript : MonoBehaviour
{
    public static string ContentFromDB { get; set; }
    public static List<ScoreFromDB> ScoreFromDBs { get; set; }

    public GameObject GameProgress;

    public void CheckIfActButSaveScore()
    {
        // Vérification que le champ de saisie du nom du joueur ne soit pas vide
        if (GameObject.FindGameObjectWithTag("PlayerDataField").transform.Find("NameField").gameObject.GetComponent<InputField>().text != "")
        {
            GameObject.FindGameObjectWithTag("SaveScoreButton").GetComponent<Button>().interactable = true;
        }
        else
        {
            GameObject.FindGameObjectWithTag("SaveScoreButton").GetComponent<Button>().interactable = false;
        }
    }

    public void SendScoreToDB()
    {
        // Récupération de la zone des infos du joueur
        GameObject playerDataFields = GameObject.FindGameObjectWithTag("PlayerDataField");

        // Récupération de la saisie du joueur
        string playerName = playerDataFields.transform.Find("NameField").gameObject.GetComponent<InputField>().text;
        int playerScore = Int32.Parse(playerDataFields.transform.Find("ScoreField").gameObject.GetComponent<Text>().text);

        Debug.Log("Nom : " + playerName + ", score : " + playerScore);

        PostScoreToDB(playerName, playerScore);
    }

    private void PostScoreToDB(string nameScore, int score)
    {
        nameScore = (nameScore != null) && (nameScore != "") ? nameScore : "Inconnu";

        WWWForm form = new WWWForm();
        form.AddField("nameScore", nameScore);
        form.AddField("scoreValue", score.ToString());
        form.AddField("dateTimeScore", DateTime.Now.ToString("G"));

        UnityWebRequest webRequest = UnityWebRequest.Post("https://portfoliokarl.000webhostapp.com/Pages/PostScoreToDB.php", form);

        webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log(webRequest.error);
        }
        else
        {
            // Log
            Debug.Log("Requête effectuée avec succés");

            GameObject.FindGameObjectWithTag("PlayerDataField").transform.Find("NameField").gameObject.GetComponent<InputField>().text = "";
            GameProgress.GetComponent<GameProgressScript>().FillBestScores();
        }
    }
}
