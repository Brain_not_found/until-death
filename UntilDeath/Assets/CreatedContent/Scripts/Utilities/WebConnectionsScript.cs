﻿// using System
using System;
using System.Collections;
using System.Collections.Generic;

// using Unity
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

// using Packages
using Newtonsoft.Json;

public class WebConnectionsScript : MonoBehaviour
{
    #region Properties
    public static string ContentFromDB { get; set; }
    public static List<ScoreFromDB> ScoreFromDBs { get; set; }

    public static WebConnectionsScript Instance;

    private bool CheckedIfExist = false;
    #endregion

    // Permet au gameObject de ne pas être détruit lors du changement de scènes
    void Awake()
    {
        if (Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
    }

    public void Update()
    {
        if (!CheckedIfExist)
        {
            foreach (GameObject foundGameObject in GameObject.FindGameObjectsWithTag("WebConnection"))
            {
                if (!foundGameObject.Equals(gameObject))
                {
                    DestroyImmediate(gameObject);
                }
            }
        }
    }

    #region Functions
    // Appel des fontions via "StartCoroutine(<nomFonction>)"
    public static void RequestDBHighScores()
    {
        ScoreFromDBs = TransformDataFromDB<List<ScoreFromDB>>("https://portfolioks.fr/ConnexionToDB.php");

        /*foreach (ScoreFromDB score in ScoreFromDBs)
        {
            Debug.Log("Joueur numéro " + (ScoreFromDBs.IndexOf(score) + 1) + " : ");
            Debug.Log("  Nom du joueur : " + score.Name);
            Debug.Log("  Score du joueur : " + score.Score);
            Debug.Log("  Date du score : " + score.DateAndTime.ToLongDateString() + " à " + score.DateAndTime.TimeOfDay);
        }*/
    }

    #region Generic functions
    private static T TransformDataFromDB<T>(string requestUrl)
    {
        // Traitement de la requête
        IEnumerator req = GetDataFromDB(requestUrl);

        // Blocage tant que la requête n'est pas terminée
        while (req.MoveNext());

        try
        {
            return JsonConvert.DeserializeObject<T>(ContentFromDB);
        }
        catch (Exception exc)
        {
            // Log
            Debug.Log("Erreur lors de la conversion du JSON de la requête web : " + exc.Message);

            // Retourne une valeur vide
            return default(T);
        }
    }

    private static IEnumerator GetDataFromDB(string requestUrl)
    {
        // Log
        Debug.Log("Début de la requête GetDataFromDB() à l'adresse : " + requestUrl);

        // Création de la requête
        UnityWebRequest webRequest = UnityWebRequest.Get(requestUrl);

        // Envoi de la requête
        yield return webRequest.SendWebRequest();

        // Log
        Debug.Log("Progression de la requête : ");

        // Renvoie true tant que la requête n'est pas terminée
        while (!webRequest.isDone)
        {
            Debug.Log(webRequest.downloadProgress * 100 + "%");

            yield return null;
        }

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log(webRequest.error);
        }
        else
        {
            // Log
            Debug.Log("Requête effectuée avec succés");

            // Attribution de la valeur de retour de requête
            ContentFromDB = webRequest.downloadHandler.text;
        }
    }

    public void StartSendingHighScore(string nameScore, int score)
    {
        StartCoroutine(PostScoreToDB(nameScore, score));
    }

    public static IEnumerator PostScoreToDB(string nameScore, int score)
    {
        nameScore = (nameScore != null) && (nameScore != "") ? nameScore : "Inconnu";

        WWWForm form = new WWWForm();
        form.AddField("nameScore", nameScore);
        form.AddField("scoreValue", score.ToString());
        form.AddField("dateTimeScore", DateTime.Now.ToString("G"));

        UnityWebRequest webRequest = UnityWebRequest.Post("https://portfoliokarl.000webhostapp.com/Pages/PostScoreToDB.php", form);

        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log(webRequest.error);
        }
        else
        {
            // Log
            Debug.Log("Requête effectuée avec succés");

            // Retour au menu principal
            SceneManager.LoadScene(0);
        }
    }
    #endregion

    #endregion
}

// Classe des scores
public class ScoreFromDB
{
    #region Properties
    public string DateAndTime { get; set; }
    public string Name { get; set; }
    public int Score { get; set; }
    #endregion
}
